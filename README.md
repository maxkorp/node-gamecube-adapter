# node-gamecube-controller

Wrapper around [node-hid](https://github.com/node-hid/node-hid) to make state
of gamecube controllers plugged in via official and mayflash adapters available.
